#!/usr/bin/env python

# Libraries and functions
from optparse import OptionParser
from sys import argv, stderr, exit
import numpy as np
import pysam
from time import time
from warnings import simplefilter

# Display help
usage = "usage: python %prog -i {BAM formatted indexed file of a chromosome} [options]"
parser = OptionParser(usage=usage, version="%prog v0.1")
parser.add_option("-i", "--input", action="store", type="string", dest="inpu", help= "BAM formatted indexed file of a chromosome")
parser.add_option("-c", "--chromosome", action="store", type="string", dest="chromosome", help= "Chromosome to be analyzed. Format: Chr:Start-End", default="")
parser.add_option("-q", "--mapping-quality", action="store", type="int", dest="map_qual", help= "Filter by mapping quality. Default = 0", default=0)
parser.add_option("-p", "--prefix", action="store", type="string", dest="prefix", help= "Output files {prefix}.{methylation_pattern}.out", default="out")
parser.add_option("-d", "--window_dynamic", action="store", type="int", dest="window_dynamic", help= "Calculates the methylation (deamination) average in a given dynamic window. Recomended = 40 ", default=0)
parser.add_option("-f", "--window_fixed", action="store", type="int", dest="window_fixed", help= "Calculates the methylation (deamination) average in a fixed given window. Default = 100000 (100kb)", default=100000)
parser.add_option("-r", "--remove", action="store", type="int", dest="remove", help= "Removes the n firsts positions of the reads, because of reduced UDG efficiency on extremes. Default = 0  Do not change it!! recomended!! ", default=0)
parser.add_option("-v", "--covfilter", action="store", type="int", dest="cov_filter", help= "Removes low coverage positions in Gokam approach. Default = 5 ", default=5)
parser.add_option("-m", "--method", action="store", type="string", dest="method", help= "Method to compute methylation 'all' bases (Gokhman et al. 2014) or 'first' base (Pedersen et al. 2014), default = 'all (Gokhman)'", default="all")
parser.add_option("-z", "--gzip", action="store_true", dest="gzipped", help= "Out matrix compressed with gzip", default=False)
parser.add_option("-t", "--threads", action="store", type="int", dest="threads", help= "Number of threads to be used, default = 1", default = 1)
parser.add_option("-s", "--silence", action="store_true", dest="silence", help= "Silence mode. Prints no messages", default=False)

(options, args) = parser.parse_args()

if options.gzipped == True:
    import gzip

simplefilter("ignore")

# Start
ini_time = time()

if len(argv) == 1:
    parser.print_help()
    exit()

if options.method == "first":
    options.cov_filter = 1

# Initial BAM file processed with pysam
inp = pysam.AlignmentFile("%s" % options.inpu, 'rb')

# Dictionaries storing frequencies
mismCG = {}; totalCG = {}; mismCHG = {}; totalCHG = {}; mismCHH = {}; totalCHH = {}

def across(input):
    # Separating the key elements of each read
    formatted_data = {}
    formatted_data["flag"] = input.flag
    formatted_data["position"] = input.reference_start
    cigar = input.cigarstring
    rawread = input.query_sequence
    mdt = input.get_tag("MD")

    # Translating the reads into reference sequences
    a = pysam.AlignedRead()
    a.query_sequence = rawread
    a.cigarstring = cigar
    a.set_tag("MD", mdt)
    rawref = (a.get_reference_sequence().upper())

    ## Reformatting read and reference sequences with CIGAR string

    cigarlis = []
    for i in range(len(cigar)):
        if cigar[i].isdigit() and cigar[i+1].isdigit() and cigar[i+2].isdigit():
            cigarlis.append(int(cigar[i:i+3]))
            cigarlis.append(cigar[i+3])
        elif cigar[i].isdigit() and cigar[i+1].isdigit() and not cigar[i+2].isdigit() and not cigar[i-1].isdigit():
            cigarlis.append(int(cigar[i:i+2]))
            cigarlis.append(cigar[i+2])
        elif cigar[i].isdigit() and not cigar[i+1].isdigit() and not cigar[i-1].isdigit():
            cigarlis.append(cigar[i])
            cigarlis.append(cigar[i+1])

    pos = 0;    formatted_data["ref"] = rawref;   formatted_data["read"] = rawread

    if len(cigarlis) == 2 and cigarlis[1] == "M":
        formatted_data["read"] = rawread;     formatted_data["ref"] = rawref
    else:
        for i in range(len(cigarlis)):
            if cigarlis[i] == "M":
                pos += int(cigarlis[i-1])
                continue
            elif cigarlis[i] == "I" or cigarlis[i] == "S":
                formatted_data["ref"] = formatted_data["ref"][:pos]+(int(cigarlis[i-1]) * "-")+formatted_data["ref"][pos:]
                pos += int(cigarlis[i-1])
                continue
            elif cigarlis[i] == "D":
                formatted_data["read"] = formatted_data["read"][:pos]+(int(cigarlis[i-1]) * "-")+formatted_data["read"][pos:]
                pos += int(cigarlis[i-1])
                continue

    return formatted_data

# Patterns forward strand
CHG_fwd = ("CAG", "CTG", "CCG")
CHH_fwd = ("CAA", "CAT", "CAC", "CTA", "CTT", "CTC", "CCA", "CCT", "CCC")

# Computation of mismatch positions and total C positions (Gokam approach)
def comp_gokam_forward(read, ref, position):
    for numnuc in range(options.remove, len(ref)):
        if ref[numnuc] == "C":
            # CG
            if ref[numnuc:numnuc+2] == "CG" and read[numnuc] == "T":
                if (int(position) + numnuc) not in mismCG:
                    mismCG[int(position) + numnuc] = 1
                    totalCG[int(position) + numnuc] = 1
                else:
                    mismCG[int(position) + numnuc] += 1
                    totalCG[int(position) + numnuc] += 1
            elif ref[numnuc:numnuc+2] == "CG" and read[numnuc] == "C":
                if (int(position) + numnuc) not in mismCG:
                    mismCG[int(position) + numnuc] = 0
                    totalCG[int(position) + numnuc] = 1
                else:
                    mismCG[int(position) + numnuc] += 0
                    totalCG[int(position) + numnuc] += 1
            # CHG
            elif ref[numnuc:numnuc+3] in CHG_fwd and read[numnuc] == "T":
                if (int(position) + numnuc) not in mismCHG:
                    mismCHG[int(position) + numnuc] = 1
                    totalCHG[int(position) + numnuc] = 1
                else:
                    mismCHG[int(position) + numnuc] += 1
                    totalCHG[int(position) + numnuc] += 1
            elif ref[numnuc:numnuc+3] in CHG_fwd and read[numnuc] == "C":
                if (int(position) + numnuc) not in mismCHG:
                    mismCHG[int(position) + numnuc] = 0
                    totalCHG[int(position) + numnuc] = 1
                else:
                    mismCHG[int(position) + numnuc] += 0
                    totalCHG[int(position) + numnuc] += 1
            # CHH
            elif ref[numnuc:numnuc+3] in CHH_fwd and read[numnuc] == "T":
                if (int(position) + numnuc) not in mismCHH:
                    mismCHH[int(position) + numnuc] = 1
                    totalCHH[int(position) + numnuc] = 1
                else:
                    mismCHH[int(position) + numnuc] += 1
                    totalCHH[int(position) + numnuc] += 1
            elif ref[numnuc:numnuc+3] in CHH_fwd and read[numnuc] == "C":
                if (int(position) + numnuc) not in mismCHH:
                    mismCHH[int(position) + numnuc] = 0
                    totalCHH[int(position) + numnuc] = 1
                else:
                    mismCHH[int(position) + numnuc] += 0
                    totalCHH[int(position) + numnuc] += 1

# Patterns reverse strand
CHG_rev = ("GTC", "GAC", "GGC")
CHH_rev = ("GTT", "GTA", "GTG", "GAT", "GAA", "GAG", "GGT", "GGA", "GGG")

def comp_gokam_reverse(read, ref, position):
    read = read[::-1]
    ref = ref[::-1]
    for numnuc in range(options.remove, len(ref)):
        if ref[numnuc] == "G":
            # CG
            if ref[numnuc:numnuc+2] == "GC" and read[numnuc] == "A":
                if (int(position) +  (numnuc)) not in mismCG:
                    mismCG[int(position) + (numnuc)] = 1
                    totalCG[int(position) + (numnuc)] = 1
                else:
                    mismCG[int(position) + (numnuc)] += 1
                    totalCG[int(position) + (numnuc)] += 1
            elif ref[numnuc:numnuc+2] == "GC" and read[numnuc] == "G":
                if (int(position) +  (numnuc)) not in mismCG:
                    mismCG[int(position) + (numnuc)] = 0
                    totalCG[int(position) + (numnuc)] = 1
                else:
                    mismCG[int(position) + (numnuc)] += 0
                    totalCG[int(position) + (numnuc)] += 1
            # CHG
            elif ref[numnuc:numnuc+3] in CHG_rev and read[numnuc] == "A":
                if (int(position) +  (numnuc)) not in mismCHG:
                    mismCHG[int(position) + (numnuc)] = 1
                    totalCHG[int(position) + (numnuc)] = 1
                else:
                    mismCHG[int(position) + (numnuc)] += 1
                    totalCHG[int(position) + (numnuc)] += 1
            elif ref[numnuc:numnuc+3] in CHG_rev and read[numnuc] == "G":
                if (int(position) +  (numnuc)) not in mismCHG:
                    mismCHG[int(position) + (numnuc)] = 0
                    totalCHG[int(position) + (numnuc)] = 1
                else:
                    mismCHG[int(position) + (numnuc)] += 0
                    totalCHG[int(position) + (numnuc)] += 1
            # CHH
            elif ref[numnuc:numnuc+3] in CHH_rev and read[numnuc] == "A":
                if (int(position) +  (numnuc)) not in mismCHH:
                    mismCHH[int(position) + (numnuc)] = 1
                    totalCHH[int(position) + (numnuc)] = 1
                else:
                    mismCHH[int(position) + (numnuc)] += 1
                    totalCHH[int(position) + (numnuc)] += 1
            elif ref[numnuc:numnuc+3] in CHH_rev and read[numnuc] == "G":
                if (int(position) +  (numnuc)) not in mismCHH:
                    mismCHH[int(position) + (numnuc)] = 0
                    totalCHH[int(position) + (numnuc)] = 1
                else:
                    mismCHH[int(position) + (numnuc)] += 0
                    totalCHH[int(position) + (numnuc)] += 1

# Computation of mismatch positions and total C positions (Pedersen approach)
def comp_pedersen_forward(read, ref, position):
    if ref[:2] == "CG" and read[0] == "T":
        if (int(position)) not in mismCG:
            mismCG[int(position)] = 1
            totalCG[int(position)] = 1
        else:
            mismCG[int(position)] += 1
            totalCG[int(position)] += 1
    elif ref[:2] == "CG" and read[0] == "C":
        if (int(position)) not in mismCG:
            mismCG[int(position)] = 0
            totalCG[int(position)] = 1
        else:
            mismCG[int(position)] += 0
            totalCG[int(position)] += 1

    elif ref[:3] == "CAG" and read[0] == "T" or ref[:3] == "CTG" and read[0] == "T" or ref[:3] == "CCG" \
            and read[0] == "T":
        if (int(position)) not in mismCHG:
            mismCHG[int(position)] = 1
            totalCHG[int(position)] = 1
        else:
            mismCHG[int(position)] += 1
            totalCHG[int(position)] += 1
    elif ref[:3] == "CAG" and read[0] == "C" or ref[:3] == "CTG" and read[0] == "C" or ref[:3] == "CCG" \
            and read[0] == "C":
        if (int(position)) not in mismCHG:
            mismCHG[int(position)] = 0
            totalCHG[int(position)] = 1
        else:
            mismCHG[int(position)] += 0
            totalCHG[int(position)] += 1

    elif ref[:3] == "CAA" and read[0] == "T" or ref[:3] == "CAT" and read[0] == "T" or ref[:3] == "CAC" \
            and read[0] == "T" or ref[:3] == "CTA" and read[0] == "T" or ref[:3] == "CTT" and read[0] == "T" \
            or ref[:3] == "CTC" and read[0] == "T" or ref[:3] == "CCA" and read[0] == "T" or ref[:3] == "CCT" \
            and read[0] == "T" or ref[:3] == "CCC" and read[0] == "T":
        if (int(position)) not in mismCHH:
            mismCHH[int(position)] = 1
            totalCHH[int(position)] = 1
        else:
            mismCHH[int(position)] += 1
            totalCHH[int(position)] += 1
    elif ref[:3] == "CAA" and read[0] == "C" or ref[:3] == "CAT" and read[0] == "C" or ref[:3] == "CAC" \
            and read[0] == "C" or ref[:3] == "CTA" and read[0] == "C" or ref[:3] == "CTT" and read[0] == "C" \
            or ref[:3] == "CTC" and read[0] == "C" or ref[:3] == "CCA" and read[0] == "C" or ref[:3] == "CCT" \
            and read[0] == "C" or ref[:3] == "CCC" and read[0] == "C":
        if (int(position)) not in mismCHH:
            mismCHH[int(position)] = 0
            totalCHH[int(position)] = 1
        else:
            mismCHH[int(position)] += 0
            totalCHH[int(position)] += 1

def comp_pedersen_reverse(read, ref, position):
    if ref[-2:] == "CG" and read[-1] == "A":
        if (int(position)) not in mismCG:
            mismCG[int(position)] = 1
            totalCG[int(position)] = 1
        else:
            mismCG[int(position)] += 1
            totalCG[int(position)] += 1
    elif ref[-2:] == "CG" and read[-1] == "G":
        if (int(position)) not in mismCG:
            mismCG[int(position)] = 0
            totalCG[int(position)] = 1
        else:
            mismCG[int(position)] += 0
            totalCG[int(position)] += 1

    elif ref[-3:] == "CTG" and read[-1] == "A" or ref[-3:] == "CAG" and read[-1] == "A" or ref[-3:] == "CGG" \
            and read[-1] == "A":
        if (int(position)) not in mismCHG:
            mismCHG[int(position)] = 1
            totalCHG[int(position)] = 1
        else:
            mismCHG[int(position)] += 1
            totalCHG[int(position)] += 1
    elif ref[-3:] == "CTG" and read[-1] == "G" or ref[-3:] == "CAG" and read[-1] == "G" or ref[-3:] == "CGG" \
            and read[-1] == "G":
        if (int(position)) not in mismCHG:
            mismCHG[int(position)] = 0
            totalCHG[int(position)] = 1
        else:
            mismCHG[int(position)] += 0
            totalCHG[int(position)] += 1

    elif ref[-3:] == "TTG" and read[-1] == "A" or ref[-3:] == "ATG" and read[-1] == "A" or ref[-3:] == "GTG" \
            and read[-1] == "A" or ref[-3:] == "TAG" and read[-1] == "A" or ref[-3:] == "AAG" and read[-1] == "A" \
            or ref[-3:] == "GAG" and read[-1] == "A" or ref[-3:] == "TTG" and read[-1] == "A" or ref[-3:] == "AGG" \
            and read[-1] == "A" or ref[-3:] == "GGG" and read[-1] == "A":
        if (int(position)) not in mismCHH:
            mismCHH[int(position)] = 1
            totalCHH[int(position)] = 1
        else:
            mismCHH[int(position)] += 1
            totalCHH[int(position)] += 1
    elif ref[-3:] == "TTG" and read[-1] == "G" or ref[-3:] == "ATG" and read[-1] == "G" or ref[-3:] == "GTG" \
            and read[-1] == "G" or ref[-3:] == "TAG" and read[-1] == "G" or ref[-3:] == "AAG" and read[-1] == "G" \
            or ref[-3:] == "GAG" and read[-1] == "G" or ref[-3:] == "TTG" and read[-1] == "G" or ref[-3:] == "AGG" \
            and read[-1] == "G" or ref[-3:] == "GGG" and read[-1] == "G":
        if (int(position)) not in mismCHH:
            mismCHH[int(position)] = 0
            totalCHH[int(position)] = 1
        else:
            mismCHH[int(position)] += 0
            totalCHH[int(position)] += 1

# Function to smooth the raw frequencies with dynamic windows. Defines a size of window depending on the neighboring pattern sites and calculates the average of both, position and frequency
def window_dynamic(pattern_out):
    if options.gzipped == False:
        inp = (open(pattern_out)).readlines()
    else:
        inp = (gzip.open(pattern_out)).readlines()

    positions = []
    mism_lis = []
    total_lis = []

    mism = {}
    total = {}

    window_size = options.window_dynamic

    for i in range(0, len(inp) - window_size, window_size + 1):
        for u in range(1, window_size + 1):
            positions.append(int((inp[u + i]).split("\t")[0]))
            mism_lis.append(int((inp[u + i]).split("\t")[1]))
            total_lis.append(int((inp[u + i]).split("\t")[2]))
        mism[sum(positions) / float(len(positions))] = sum(mism_lis) / float(len(mism_lis))
        total[sum(positions) / float(len(positions))] = sum(total_lis) / float(len(total_lis))
        positions = []
        mism_lis = []
        total_lis = []

    meth = {}
    for i in mism:
        meth[i] = mism[i] / (total[i])

    if "cg" in pattern_out:
        cg_out = open("%s.cg.window_dynam%s.txt" % (options.prefix, window_size), "w")
        cg_out.write("Position\tCG_Methylation_proxy\n")
        for i in sorted(meth):
            cg_out.write("%s\t%s\n" % (i, meth[i] * 100))
    elif "chg" in pattern_out:
        chg_out = open("%s.chg.window_dynam%s.txt" % (options.prefix, window_size), "w")
        chg_out.write("Position\tCHG_Methylation_proxy\n")
        for i in sorted(meth):
            chg_out.write("%s\t%s\n" % (i, meth[i] * 100))
    elif "chh" in pattern_out:
        chh_out = open("%s.chh.window_dynam%s.txt" % (options.prefix, window_size), "w")
        chh_out.write("Position\tCHH_Methylation_proxy\n")
        for i in sorted(meth):
            chh_out.write("%s\t%s\n" % (i, meth[i] * 100))
    else:
        stderr.write("Error")

# Function to calculate the average methylation in a given fixed window.
def window_fixo(pattern_out):
    inp = np.genfromtxt(pattern_out, delimiter="\t", skip_header=True)

    window_s = options.window_fixed

    meto = np.array([])
    data = np.array([], dtype=int)

    for i in range(0, int(inp[-1, 0]), window_s):
        meto = np.append(meto,
                         (np.mean(inp[(np.where((inp[:, 0] >= i) & (inp[:, 0] < i + window_s))), 3])) * 100)
        data = np.append(data, (len(inp[(np.where((inp[:, 0] >= i) & (inp[:, 0] < i + window_s)))])))

    if "cg" in pattern_out:
        cg_out = open("%s.cg.window_fix%s.txt" % (options.prefix, window_s), "w")
        cg_out.write("Window\tCG_Methylation_proxy\tData_per_window\n")
        for i in range(len(data)):
            cg_out.write("%s\t%s\t%s\n" % (i * 100000, meto[i], data[i]))
    elif "chg" in pattern_out:
        chg_out = open("%s.chg.window_fix%s.txt" % (options.prefix, window_s), "w")
        chg_out.write("Window\tCHG_Methylation_proxy\tData_per_window\n")
        for i in range(len(data)):
            chg_out.write("%s\t%s\t%s\n" % (i * 100000, meto[i], data[i]))
    elif "chh" in pattern_out:
        chh_out = open("%s.chh.window_fix%s.txt" % (options.prefix, window_s), "w")
        chh_out.write("Window\tCHH_Methylation_proxy\tData_per_window\n")
        for i in range(len(data)):
            chh_out.write("%s\t%s\t%s\n" % (i * 100000, meto[i], data[i]))
    else:
        stderr.write("Error")

# MAIN CONTROL
if options.chromosome == "":
    exit("Empty option -c (--chromosome). Please set chromosome (and region). Format: Chr:Start-End")

chrom = ((options.chromosome).split(":"))[0]
if ((options.chromosome).split(":"))[1] == "" :
    for line in inp.fetch(chrom):
        if line.mapping_quality >= options.map_qual:
            result_across = across(line)
            if options.method == "all":
                if result_across["flag"] == 0:
                    comp_gokam_forward(result_across["read"], result_across["ref"], result_across["position"])
                elif result_across["flag"] == 16:
                    comp_gokam_reverse(result_across["read"], result_across["ref"], result_across["position"])
            elif options.method == "first":
                if result_across["flag"] == 0:
                    comp_pedersen_forward(result_across["read"], result_across["ref"], result_across["position"])
                elif result_across["flag"] == 16:
                    comp_pedersen_reverse(result_across["read"], result_across["ref"], result_across["position"])
            else:
                    exit("Please select a correct method: 'all' or 'first'. Default='all'")

else:
    chrom_ini = int(((((options.chromosome).split(":"))[1]).split("-"))[0])
    chrom_end = int(((((options.chromosome).split(":"))[1]).split("-"))[1])
    for line in inp.fetch(chrom, chrom_ini, chrom_end):
        if line.mapping_quality >= options.map_qual:
            result_across = across(line)
            if options.method == "all":
                if result_across["flag"] == 0:
                    comp_gokam_forward(result_across["read"], result_across["ref"], result_across["position"])
                elif result_across["flag"] == 16:
                    comp_gokam_reverse(result_across["read"], result_across["ref"], result_across["position"])
            elif options.method == "first":
                if result_across["flag"] == 0:
                    comp_pedersen_forward(result_across["read"], result_across["ref"], result_across["position"])
                elif result_across["flag"] == 16:
                    comp_pedersen_reverse(result_across["read"], result_across["ref"], result_across["position"])
            else:
                    exit("Please select a correct method: 'all' or 'first'. Default='all'")

# Output files with raw frequencies
if options.gzipped == False:
    cg_out = open("%s.cg.out" % options.prefix, "w")
    cg_out.write("Pos\tCG-T_Mism\tCG_Total\tMeth_CG\n")
    for i in sorted(mismCG):
        if i in totalCG and int(totalCG[i]) >= options.cov_filter:
            cg_out.write("%s\t%s\t%s\t%s\n" % (i, mismCG[i], totalCG[i], (float(mismCG[i]) / totalCG[i])))
    cg_out.close()

    chg_out = open("%s.chg.out" % options.prefix, "w")
    chg_out.write("Pos\tCHG-T_Mism\tCHG_Total\tMeth_CHG\n")
    for i in sorted(mismCHG):
        if i in totalCHG and int(totalCHG[i]) >= options.cov_filter:
            chg_out.write("%s\t%s\t%s\t%s\n" % (i, mismCHG[i], totalCHG[i], (float(mismCHG[i]) / totalCHG[i])))
    chg_out.close()

    chh_out = open("%s.chh.out" % options.prefix, "w")
    chh_out.write("Pos\tCHH-T_Mism\tCHH_Total\tMeth_CHH\n")
    for i in sorted(mismCHH):
        if i in totalCHH and int(totalCHH[i]) >= options.cov_filter:
            chh_out.write("%s\t%s\t%s\t%s\n" % (i, mismCHH[i], totalCHH[i], (float(mismCHH[i]) / totalCHH[i])))
    chh_out.close()
else:
        cg_out = gzip.open("%s.cg.out.gz" % options.prefix, "wb")
        cg_out.write(str.encode("Pos\tCG-T_Mism\tCG_Total\tMeth_CG\n"))
        for i in sorted(mismCG):
            if i in totalCG and int(totalCG[i]) >= options.cov_filter:
                cg_out.write(str.encode("%s\t%s\t%s\t%s\n" % (i, mismCG[i], totalCG[i], (float(mismCG[i]) / totalCG[i]))))
        cg_out.close()

        chg_out = gzip.open("%s.chg.out.gz" % options.prefix, "wb")
        chg_out.write(str.encode("Pos\tCHG-T_Mism\tCHG_Total\tMeth_CHG\n"))
        for i in sorted(mismCHG):
            if i in totalCHG and int(totalCHG[i]) >= options.cov_filter:
                chg_out.write(str.encode("%s\t%s\t%s\t%s\n" % (i, mismCHG[i], totalCHG[i], (float(mismCHG[i]) / totalCHG[i]))))
        chg_out.close()

        chh_out = gzip.open("%s.chh.out.gz" % options.prefix, "wb")
        chh_out.write(str.encode("Pos\tCHH-T_Mism\tCHH_Total\tMeth_CHH\n"))
        for i in sorted(mismCHH):
            if i in totalCHH and int(totalCHH[i]) >= options.cov_filter:
                chh_out.write(str.encode("%s\t%s\t%s\t%s\n" % (i, mismCHH[i], totalCHH[i], (float(mismCHH[i]) / totalCHH[i]))))
        chh_out.close()

# Takes each raw output file and inputs it into the smooth function of dynamic window
if options.window_dynamic != 0:
    if options.gzipped == False:
        window_dynamic("%s.cg.out" % options.prefix)
        window_dynamic("%s.chg.out" % options.prefix)
        window_dynamic("%s.chh.out" % options.prefix)
    else:
        window_dynamic("%s.cg.out.gz" % options.prefix)
        window_dynamic("%s.chg.out.gz" % options.prefix)
        window_dynamic("%s.chh.out.gz" % options.prefix)

# Takes each raw output file and inputs it into the smooth function of fixed window
if options.window_fixed:
    if options.gzipped == False:
        window_fixo("%s.cg.out" % options.prefix)
        window_fixo("%s.chg.out" % options.prefix)
        window_fixo("%s.chh.out" % options.prefix)
    else:
        window_fixo("%s.cg.out.gz" % options.prefix)
        window_fixo("%s.chg.out.gz" % options.prefix)
        window_fixo("%s.chh.out.gz" % options.prefix)

# Prints the names of the output files in standard error
if options.silence == False:
    stderr.write("-----------%s Seconds-----------\n" % int(time()-ini_time))
    stderr.write("Method implemented:\t%s base(s)\n" % options.method)
    stderr.write("Chromosome and region analyzed:\t%s\n" % options.chromosome)
    if options.remove:
        stderr.write("Number of nucleotides removed at firsts positions:\t%s\n" % options.remove)
    stderr.write("CG raw frequencies file:\t%s.cg.out\n" % options.prefix)
    stderr.write("CHG raw frequencies file:\t%s.chg.out\n" % options.prefix)
    stderr.write("CHH raw frequencies file:\t%s.chh.out\n" % options.prefix)
    stderr.write("\n")
    if options.window_dynamic != 0:
        stderr.write("Dynamic window size:\t%s\n" % options.window_dynamic)
        stderr.write("CG dynamic windows file:\t%s.cg.window_dynam%s.txt\n" % (options.prefix, options.window_dynamic))
        stderr.write("CHG dynamic windows file:\t%s.chg.window_dynam%s.txt\n" % (options.prefix, options.window_dynamic))
        stderr.write("CHH dynamic windows file:\t%s.chh.window_dynam%s.txt\n" % (options.prefix, options.window_dynamic))
        stderr.write("\n")
    stderr.write("Fixed window size:\t%s\n" % options.window_fixed)
    stderr.write("Coverage filter:\t%s\n" % options.cov_filter)
    stderr.write("\n")
    stderr.write("CG fixed windows file:\t%s.cg.window_fix%s.txt\n" % (options.prefix, options.window_fixed))
    stderr.write("CHG fixed windows file:\t%s.chg.window_fix%s.txt\n" % (options.prefix, options.window_fixed))
    stderr.write("CHH fixed windows file:\t%s.chh.window_fix%s.txt\n" % (options.prefix, options.window_fixed))
