# plant_ancient_methyl_calculator

## Description

Software tool that calculates methylation proxies in a given window size from an indexed bam file

## Installation

```bash
git clone https://github.com/smlatorreo/plant_ancient_methyl_calculator.git
```

## Excecuting

```bash
cd plant_ancient_methyl_calculator/
python ancient_plant_methylation_calculator.py -i <indexed bam file> -c <Chr:Start-End>
```
|Option|Description|
|:---|:---|
|--version|how program's version number and exit|
|-h, --help|show this help message and exit|
|-i INPU, --input=INPU|	BAM formatted indexed file of a chromosome|
|-c CHROMOSOME, --chromosome=CHROMOSOME|Chromosome to be analyzed. Format: Chr:Start-End|
|-q MAP_QUAL, --mapping-quality=MAP_QUAL|Filter by mapping quality. Default = 0|
|-p PREFIX, --prefix=PREFIX|Output files {prefix}.{methylation_pattern}.out|
|-d WINDOW_DYNAMIC, --window_dynamic=WINDOW_DYNAMIC|Calculates the methylation (deamination) average in a given dynamic window. Recomended = 40|
|-f WINDOW_FIXED, --window_fixed=WINDOW_FIXED|Calculates the methylation (deamination) average in a fixed given window. Default = 100000 (100kb)|
|-r REMOVE, --remove=REMOVE|Removes the n firsts positions of the reads, because of reduced UDG efficiency on extremes. Default = 0  Do not change it!! recomended!!|
|-v COV_FILTER, --covfilter=COV_FILTER|	Removes low coverage positions in Gokam approach. Default = 5|
|-m METHOD, --method=METHOD|Method to compute methylation 'all' bases (Gokhman et al. 2014) or 'first' base (Pedersen et al. 2014), default = 'all (Gokhman)'|
|-z, --gzip|Out matrix compressed with gzip|
|-t THREADS, --threads=THREADS|	Number of threads to be used, default = 1|
|-s, --silence|	Silence mode. Prints no messages|

## Dependencies
+ python3 (recommended) or python2
+ pysam
+ numpy

## License
MIT
## Author
Sergio Latorre
## Contact
smlatorreo@gmail.com
